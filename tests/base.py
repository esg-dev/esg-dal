#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of esg-dal.
# https://gitlab.com/esg-dev/esg-dal

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2015, Tal Peretz <13@1500.co.il>

from unittest import TestCase as PythonTestCase


class TestCase(PythonTestCase):
    pass
