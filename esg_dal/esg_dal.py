# -*- coding: utf-8 -*-
import sqlalchemy
from sqlalchemy.engine import create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import update, insert

class EsgDal(object):
    def __init__(self, tables_list=None):
        if tables_list is None:
            tables_list = []
        self.tables_list = tables_list
        self.session = None
        self.engine = None
        self.base = None
        self.connect_to_db().db_auto_mapping()
        self.metadata = sqlalchemy.MetaData(bind=self.engine)

    def connect_to_db(self, user_name='esguser', password='esg1', host='192.168.10.112', db='ESG'):
        self.engine = create_engine('mssql+pymssql://{user_name}:{password}@{host}/{db}'.format(user_name=user_name,
                                                                                                password=password,
                                                                                                host=host,
                                                                                                db=db))
        self.engine.connect()
        self.session = sessionmaker(bind=self.engine)()
        return self

    def db_auto_mapping(self):
        metadata = sqlalchemy.MetaData()
        metadata.reflect(self.engine, only=self.tables_list)
        self.base = automap_base(metadata=metadata)
        self.base.prepare()

    def set_tables(self, tables_list):
        self.tables_list = tables_list

    def __getitem__(self, item):
        return self.table_to_class(item)

    def table_to_class(self, table_name):
        return self.base.classes[table_name]

    def query(self, table_name):
        table = self[table_name]
        return self.session.query(table)

    def get_table_by_name(self, table_name):
        return sqlalchemy.Table(table_name, self.metadata, autoload=True)

    def insert(self, table_name, values):
        table_to_update = self.get_table_by_name(table_name)
        self.session.execute(insert(table_to_update).values(values))
        self.session.commit()

    def update(self, table_name, where_col, where_val, new_values):
        table_to_update = self.get_table_by_name(table_name)
        self.session.execute(update(table_to_update).values(new_values).where(table_to_update.c[where_col] == where_val))
        self.session.commit()