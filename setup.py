#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of esg-dal.
# https://gitlab.com/esg-dev/esg-dal

# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT-license
# Copyright (c) 2015, Tal Peretz <13@1500.co.il>

from setuptools import setup, find_packages
from pip.req import parse_requirements
install_reqs = parse_requirements('requirements.txt', session=False)
reqs = [str(ir.req) for ir in install_reqs]
__version__ = '0.1.0'

tests_require = [
    'mock',
    'nose',
    'coverage',
    'yanc',
    'tox',
    'coveralls',
    'sphinx',
]

setup(
    name='esg-dal',
    version=__version__,
    description='Esg DAL',
    long_description='''
Esg DAL
''',
    keywords='DAL ESG',
    author='Tal Peretz',
    author_email='13@1500.co.il',
    url='https://gitlab.com/esg-dev/esg-dal',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: Unix',
        'Programming Language :: Python :: 2.7',
        'Operating System :: OS Independent',
    ],
    packages=find_packages(),
    include_package_data=True,
    install_requires=reqs,
    extras_require={
        'tests': tests_require,
    },
    entry_points={
        'console_scripts': [
            # add cli scripts here in this form:
            # 'esg-dal=esg_dal.cli:main',
        ],
    },
)
