import json
import datetime
from sandman import app
from flask import request, Response

from esg_dal import EsgDal

app.config['SQLALCHEMY_DATABASE_URI'] = 'mssql+pymssql://esguser:esg1@192.168.10.112/ESG'

from sandman.model import register, Model, activate

dal = EsgDal(['tblCustomers'])


class GasLogin(Model):
    __tablename__ = 'GasLogin'


class GasCompany(Model):
    __tablename__ = 'GasCompany'


class tblCustomers(Model):
    __tablename__ = 'tblCustomers'

    def as_dict(self, depth=0, cols_to_remove=['SSMA_TimeStamp']):
        return super(tblCustomers, self).as_dict(depth=depth, cols_to_remove=cols_to_remove)


class tblworkingPage(Model):
    __tablename__ = 'tblworkingPage'

    def as_dict(self, depth=0, cols_to_remove=['SSMA_TimeStamp']):
        return super(tblworkingPage, self).as_dict(depth=depth, cols_to_remove=cols_to_remove)


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            encoded_object = list(o.timetuple())[0:6]
        else:
            encoded_object = json.JSONEncoder.default(self, o)
        return encoded_object


def serialize_working_page_object(working_page):
    return {'ID': working_page.ID,
            'KodCustomer': working_page.KodCustomer,
            'SubjectDetailes': working_page.SubjectDetailes,
            'SubjectDetailesCode': working_page.SubjectDetailesCode,
            'KodSecondSubject': working_page.KodSecondSubject,
            'CustomerConnectionMan': working_page.CustomerConnectionMan,
            'SubjectConnectionMan': working_page.SubjectConnectionMan,
            'SubjectConnectionCompany': working_page.SubjectConnectionCompany,
            'monthlyQuantity': working_page.monthlyQuantity,
            'modeQuantity': working_page.modeQuantity,
            'currentCondition': working_page.currentCondition,
            'offerCondition': working_page.offerCondition,
            'DBcondition': working_page.DBcondition,
            'discontMonthly': working_page.discontMonthly,
            'sumOfCredit': working_page.sumOfCredit,
            'feeService': working_page.feeService,
            'remarkCollection': working_page.remarkCollection,
            'remark': working_page.remark,
            'treatmentDate': working_page.treatmentDate,
            'endDateAdvisoring': working_page.endDateAdvisoring,
            'invoiceNum': working_page.invoiceNum,
            'KodResponsible': working_page.KodResponsible,
            'endRow': working_page.endRow,
            'notRelevante': working_page.notRelevante,
            'waitingStatus': working_page.waitingStatus,
            'IDagreemen': working_page.IDagreemen}


def serialize_worker_object(worker):
    return {'NumWorker': worker.NumWorker,
            'LastName': worker.LastName,
            'FirstName': worker.FirstName,
            'Extension': worker.Extension,
            'Telephone': worker.Telephone,
            'Pelephone': worker.Pelephone,
            'Address': worker.Address,
            'BirthDate': worker.BirthDate,
            'EMail': worker.EMail,
            'EMailNumeric': worker.EMailNumeric,
            'ID': worker.ID,
            'Role': worker.Role,
            'NumTeam': worker.NumTeam,
            'NumGroups': worker.NumGroups,
            'active': worker.active,
            'GenderIsMale': worker.GenderIsMale,
            'hourReport': worker.hourReport,
            'dateReport': worker.dateReport}


def serialize_customer_object(customer):
    return {str(attr): getattr(customer, attr) for attr in dir(customer)
            if not attr.startswith('__') and not attr.startswith('_') and attr not in ['SSMA_TimeStamp',
                                                                                       '_sa_class_manager',
                                                                                       'prepare', 'classes',
                                                                                       'metadata']}


def serialize_team_object(team):
    return {'KodTeamCare': team.KodTeamCare, 'NameTeam': team.NameTeam}


@app.route('/search')
def search():
    serialisers = {'tblCustomers': serialize_customer_object,
                   'tblWorkers': serialize_worker_object,
                   'tblworkingPage': serialize_working_page_object,
                   'tblTeamList': serialize_team_object}
    table_name = request.args.get('table_name')
    col_name = request.args.get('col_name')
    value = request.args.get('value')
    results = dal.query(table_name).filter(getattr(dal[table_name], col_name).like('%' + value + '%')).all()
    results = [serialisers[table_name](item) for item in results]
    return Response(response=json.dumps(results, cls=DateTimeEncoder, ensure_ascii=False), status=200,
                    content_type="application/json")


register((tblCustomers, tblworkingPage))
activate(browser=False)

app.run(debug=True)
